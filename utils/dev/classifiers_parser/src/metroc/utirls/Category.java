package metroc.utirls;

import java.util.ArrayList;
import java.util.List;

public class Category {

	public static int counter = 1;
	
	public int id;
	
	public String name;
	
	public List<Subcategory> subcategories = new ArrayList<Subcategory>();

	public String content;
	
	public static final String START_HEADER_LABEL = "\">";
	public static final String END_HEADER_LABEL = "</a>";
	
	public static final String START_SUBCAT_LABEL = "\">";
	
	public static final String END_SUBCAT_LABEL = "</a></li>";
	
	
	
	@Override
	public String toString() {
		String string = "    - !!models.Category\n";
		string += 		"        id:         " + id + "\n";
		string += 		"        name:       " + name + "\n";
		return string;
	}
	
	public Category() {
		id = getNextIndex();
	}
	
	public static int getNextIndex() {
		return counter++;
	}
	
	public static Category parse(String incontent) {

		Category category = new Category();
		category.content = incontent;
		
		int startHeaderIndex = incontent.indexOf(START_HEADER_LABEL) + START_HEADER_LABEL.length();
		int endHeaderIndex = incontent.indexOf(END_HEADER_LABEL, startHeaderIndex);
		category.name = incontent.substring(startHeaderIndex, endHeaderIndex);
		
		int curIndex = endHeaderIndex + END_HEADER_LABEL.length();
		while(curIndex >= 0) {
			int startIndex = incontent.indexOf(START_SUBCAT_LABEL, curIndex) ;
			if(startIndex > 0) {
				startIndex += START_HEADER_LABEL.length();
				int endIndex =  incontent.indexOf(END_SUBCAT_LABEL, startIndex);
				if(endIndex > 0) {
					Subcategory subcategory = new Subcategory();
					subcategory.category = category;
					subcategory.name = incontent.substring(startIndex, endIndex);
					curIndex = endIndex + END_SUBCAT_LABEL.length();
					category.subcategories.add(subcategory);
				} else {
					throw new UnsupportedOperationException("Error while parse subcatory with content: " + incontent); 
				}
			} else {
				break;
			}
		}
		
		return category;
	}
	
	
}
