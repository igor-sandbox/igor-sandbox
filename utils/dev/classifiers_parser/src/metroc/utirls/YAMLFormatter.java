package metroc.utirls;

import java.util.ArrayList;
import java.util.List;

public class YAMLFormatter {

	public List<Category> categories = new ArrayList<Category>();
	
	public static final String START_LABEL = "<h3>";
	public static final String END_LABEL = "</ul>"; 
	
	@Override
	public String toString() {
		String string = "# Categories\n\n";
		string += "category:\n\n";

		for(Category cat : categories) {
			string += cat.toString() + "\n";
		}

		string += "\n# Subcategories\n\n";
		string += "subcategory:\n\n";

		for(Category cat : categories) {
			for(Subcategory subcat : cat.subcategories) {
				string += subcat.toString();
			}
		}
		
		return string;
	}
	
	public void parse(String incontent) {
		int curIndex = 0;
		while(curIndex >= 0) {
			int startIndex = incontent.indexOf(START_LABEL, curIndex);
			if(startIndex > 0) {
				int endIndex = incontent.indexOf(END_LABEL, startIndex + START_LABEL.length());
				if(endIndex > 0) {
					String categoryContent = incontent.substring(startIndex, endIndex);
					Category category = Category.parse(categoryContent);
					if(category != null) {
						 categories.add(category);
					} else {
						throw new UnsupportedOperationException("Wrong category parsing invokation!"); 
					}
					curIndex = endIndex + END_LABEL.length();
				} else {
					throw new UnsupportedOperationException("Start must have end in yaml formatter!");
				}
			} else {
				break;
			}
		}
	}
	
}
