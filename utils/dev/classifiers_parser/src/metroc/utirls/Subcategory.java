package metroc.utirls;

public class Subcategory {

	public static int counter = 1;
	
	public int id;
	
	public Category category;
	
	public String name;

	
	public Subcategory() {
		id = getNextIndex();
	}
	
	public static int getNextIndex() {
		return counter++;
	}
	
	@Override
	public String toString() {
		String string = "    - !!models.Subcategory\n";
		string += 		"        id:         " + id + "\n";
		string += 		"        name:       " + name + "\n";
		string += 		"        category:   !!models.Category\n";
		string += 		"                        id: " + category.id + "\n\n";
		return string;
	}

	
}
