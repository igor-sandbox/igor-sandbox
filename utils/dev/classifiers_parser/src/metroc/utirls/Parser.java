package metroc.utirls;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Parser {

	public static String filename = "/mnt/second/igor/work/last/igor-sandbox/metroc_utils/index.html";

	public static void main(String[] args) {
		String content = readfile();
		YAMLFormatter formatter = new YAMLFormatter();
		formatter.parse(content);
		System.out.println(formatter.toString());
		
		
	}

	
	
	
	public static String readfile() {
		File file = new File(filename);
		BufferedReader br = null;
		InputStreamReader isr = null;
		FileInputStream fis = null;
		StringBuffer sb = new StringBuffer();
		try {
			
		    fis = new FileInputStream(file);
		    isr = new InputStreamReader(fis, "UTF-8");
		    br = new BufferedReader(isr);
		    
		    String line = br.readLine();
			while (line != null) {
				sb.append(line + "\n");
				line = br.readLine();
			}
			
			br.close();
			isr.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

}
