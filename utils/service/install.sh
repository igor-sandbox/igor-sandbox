#!/bin/bash

REPO=`readlink -f \`dirname $0\`/../../`;
APP_NAME="metroc";
APP_REPO_PATH=$REPO"/metroc";

PLAY_URL="http://download.playframework.org/releases/play-2.0.4.zip";


echo -n "Enter directory to install service and press [ENTER]: ";
read INSTALL_DIR;

#if [ -d $INSTALL_DIR ]; then
#	echo "WARNING: Directory \"$INSTALL_DIR\" already exists. Do you want to process installation.";
#fi;

mkdir -p $INSTALL_DIR;
if [ $? -ne 0 ]; then
	echo "ERROR: Can't create installation directory: \"$INSTALL_DIR\".";
	exit 1;
fi;

INST_APP=$INSTALL_DIR"/service";
START_SCRIPT=$INSTALL_DIR"/start.sh";
INST_PLAY_SRC=$INSTALL_DIR"/play-2.0.4";
INST_PLAY=$INSTALL_DIR"/play";
INST_PLAY_FILE_ZIP=$INSTALL_DIR"/play.zip";


cp -r $APP_REPO_PATH $INSTALL_DIR/;
if [ $? -ne 0 ]; then
	echo "ERROR: Can't copy files from repo dir \"$APP_REPO_NAME\" to installation dir: \"$INSTALL_DIR\".";
	exit 1;
fi;


#
# Downloading play framework 2
#
wget $PLAY_URL -O $INST_PLAY_FILE_ZIP;
if [ $? -ne 0 ]; then
	echo "ERROR: Can't download play framewrok 2 from  \"$PLAY_URL\" to: \"$INST_PLAY_FILE_ZIP\".";
	exit 1;
fi;

#
# Unzipping play  framework 2
#
unzip $INST_PLAY_FILE_ZIP -d $INSTALL_DIR;
if [ $? -ne 0 ]; then
	echo "ERROR: Can't unzip play framewrok 2 from  \"$INST_PLAY_FILE_ZIP\" to: \"$INST_PLAY_SRC\".";
	exit 1;
fi;

#
# Move play framework to normal dir
#
mv $INST_PLAY_SRC $INST_PLAY;
if [ $? -ne 0 ]; then
	echo "ERROR: Can't move play framewrok 2 from  \"$INST_PLAY_SRC\" to: \"$INST_PLAY\".";
	exit 1;
fi;


#
# create DB: TODO
# 


#
# Create start script
#
touch $START_SCRIPT;
chmod +x $START_SCRIPT;
echo "#!/bin/bash" >> $START_SCRIPT;
echo "cd ./service;" >> $START_SCRIPT;
echo "../play/play run &" >> $START_SCRIPT;


