package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.ebean.Model;

@Entity
public class Subcategory extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	public String description;
	
        @ManyToOne
        public Category category; 

	@ManyToMany(mappedBy = "subcategories")
	public Set<User> users = new HashSet<User>(); 

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
	public List<Message> messages;

        public static Finder<Long, Subcategory> find = new Finder<Long, Subcategory>(Long.class,
                        Subcategory.class);

        public static List<Subcategory> all() {
                return find.all();
        }


}
