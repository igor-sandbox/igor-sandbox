package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import play.db.ebean.Model;
import controllers.CreateMsgForm;

@Entity
public class Message extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public User owner; // not requires for public messages

	public String message;

	public String type; // public or not etc.

	public String role; // zakaz or service

	@OneToOne
	public Subcategory subcategory; // subcategory for zakaz messages

	public String contacts; // contacts in case of anonym

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "msg")
	public List<File> files; // attachements

	public String status; // message status

	public String date; // date of message

	@ManyToOne
	public User assigner; // ispolnitel

	public static Finder<Long, Message> find = new Finder<Long, Message>(
			Long.class,
			Message.class);

	public static void add(String login, CreateMsgForm objForm) {
		User user = User.findByLogin(login);
		Message msg = new Message();
		msg.owner = user;
		msg.message = objForm.message;
		user.messages.add(msg);
		msg.save();
	}

	public static List<Message> findForLogin(String login) {
		User user = User.findByLogin(login);
		return user.messages;
	}
}
