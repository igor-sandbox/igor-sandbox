package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.db.ebean.Model;

@Entity
public class Category extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	public String description;

        @OneToMany(cascade=CascadeType.ALL, mappedBy="category")
        public List<Subcategory> subcategories;

        public static Finder<Long, Category> find = new Finder<Long, Category>(Long.class,
                        Category.class);

        public static List<Category> all() {
                return find.all();
        }


}
