package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import play.data.Form;
import play.db.ebean.Model;

import com.avaje.ebean.ExpressionList;

import controllers.Helper;
import controllers.LoginUserForm;
import controllers.RegisterUserForm;


@Entity
public class User extends Model {

	public static final String ROLE_DEFAULT = "user";
	
	public static final String ROLE_ADMIN = "admin";



	public static final String USER_CUSTOMER = "customer";

	public static final String USER_COMPANY = "company";



	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String role; // system role

	public String login;

	public String hash;

	public String email; 

	public String name; // contact user name

	public String sname; // contact user second name

	public String cname; // company name

	public String type; // customer or company

	public String description; // description for companies

	@ManyToMany(cascade=CascadeType.ALL) 
	public Set<Subcategory> subcategories = new HashSet<Subcategory>();  // list of company subcategories

	public String region; // automatic detect region

	public String address; // company address

	@OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
	public List<Message> messages;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="assigner")
	public List<Message> amessages;

	public static Finder<Long, User> find = new Finder<Long, User>(Long.class,
			User.class);

	public static User findUserByLoginAndHash(Form<LoginUserForm> form) {
		String login = Helper.getValue(form, "login");
		String password = Helper.getValue(form, "password");
		ExpressionList<User> exprList = find.where().eq("login", login)
				.eq("hash", calculateHash(login, password));
		return exprList.findUnique();
	}

	public static String calculateHash(String login, String password) {
		return Helper.getMD5(login + password);
	}

	public static void register(RegisterUserForm objForm) {
		User user = new User();
		user.role = ROLE_DEFAULT;
		user.login = objForm.login;
		user.email = objForm.email;
		user.hash = calculateHash(user.login, objForm.password);
		user.save();
	}

	public static User findByLogin(String login) {
		return find.where().eq("login", login).findUnique();
	}

	public static List<User> all() {
		return find.all();
	}

	public static User findUserByLogin(Form<RegisterUserForm> form) {
		String login = Helper.getValue(form, "login");
		ExpressionList<User> exprList = find.where().eq("login", login);
		return exprList.findUnique();
	}

	 public static User findUserByEmail(Form<RegisterUserForm> form) {
		String email = Helper.getValue(form, "email");
		ExpressionList<User> exprList = find.where().eq("email", email);
		return exprList.findUnique();
	}

}
