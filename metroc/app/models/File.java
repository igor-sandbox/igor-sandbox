package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;


@Entity
public class File extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

    @ManyToOne
	public Message msg; 

	public String name; // file name with corresponding extension

	public String description;

	public String data;



}
