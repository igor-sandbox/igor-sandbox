import java.util.List;
import java.util.Map;

import models.User;
import models.Subcategory;
import models.Category;

import play.Application;
import play.GlobalSettings;
import play.libs.Yaml;

import com.avaje.ebean.Ebean;

public class Global extends GlobalSettings {

    public void onStart(Application app) {
        InitialData.insert(app);
    }

    static class InitialData {

        public static void insert(Application app) {
            Map<String,List<Object>> all = null;

            if(Ebean.find(User.class).findRowCount() == 0) {

                all = (Map<String,List<Object>>)Yaml.load("initial-data.yml");
                Ebean.save(all.get("users"));

            } 

	    if(Ebean.find(Category.class).findRowCount() == 0) {

		if(all == null) {
	                all = (Map<String,List<Object>>)Yaml.load("initial-data.yml");
	        }
                Ebean.save(all.get("category"));

	    }

	    if(Ebean.find(Subcategory.class).findRowCount() == 0) {

		if(all == null) {
	                all = (Map<String,List<Object>>)Yaml.load("initial-data.yml");
	        }
                Ebean.save(all.get("subcategory"));

	    }



        }

    }

}
