package controllers;

import models.User;
import play.data.Form;

public class RegisterUserForm {

	public String login;

	public String email;

	public transient String password;

	public transient String repassword;

	public static boolean validateForm(Form<RegisterUserForm> form) {

		boolean isHaveErrors = false;

		if (Helper.addErrorIfEmptyField(form, "login",
				"Логин должен быть заполнен"))
			isHaveErrors = true;

		if (Helper.addErrorIfEmptyField(form, "email",
				"Почта должна быть заполнена"))
			isHaveErrors = true;

		if (Helper.addErrorIfEmptyField(form, "password",
				"Пароль должен быть заполнен"))
			isHaveErrors = true;

		if (Helper.addErrorIfEmptyField(form, "repassword",
				"Пароль должен быть заполнен"))
			isHaveErrors = true;

		if (!isHaveErrors
				&& Helper.addErrorIfNotEqualsFields(form, "password",
						"repassword", "Пароли должны совпадать"))
			isHaveErrors = true;
		
		if (!isHaveErrors) {
			User user = User.findUserByLogin(form);
			if (user == null) {
				Helper.addFormError(form, "Выбранный логин занят, выберите, пожалуйста другой.");
				isHaveErrors = true;
			}  else {
				user = User.findUserByEmail(form);
				if (user == null) {
					Helper.addFormError(form, "Пользователь с такой почтой уже зарегистрирован.");
					isHaveErrors = true;
				}
			}
		}

		// TODO: Проверка на существование такого пользователя
		return isHaveErrors;
	}

}
