package controllers;


import java.util.List;

import models.Message;
import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.addmsg;
import views.html.admin;
import views.html.profile;
import views.html.aprofile;



@Security.Authenticated(Secured.class)
public class InnerApp extends Controller {


	
	public static Result addMsgPage() {
		return ok(addmsg.render(form(CreateMsgForm.class)));
	}
	
	public static Result addMsg() {
		Form<CreateMsgForm> form = form(CreateMsgForm.class).bindFromRequest();
		CreateMsgForm.validateForm(form);
		if (form.hasErrors()) {
			return badRequest(addmsg.render(form));
		}

		CreateMsgForm objForm = form.get();
		Message.add(request().username(), objForm);
		return redirect(routes.InnerApp.profile());
	}
	
	
	public static Result aprofile(String login) {
		User ownerUser = User.findByLogin(request().username());
	
		if(!ownerUser.role.equals(User.ROLE_ADMIN))
			return redirect(routes.InnerApp.profile());
		
		User user = User.findByLogin(login);
		return ok(aprofile.render(user));
	}
	
	public static Result profile() {
		List<Message> msgs = Message.findForLogin(request().username());
		return ok(profile.render(msgs));
	}
	
	public static Result admin() {
		User user = User.findByLogin(request().username());

		if(!user.role.equals(User.ROLE_ADMIN)) 
			return redirect(routes.InnerApp.profile());

		List<User> users = User.all();
		return ok(admin.render(users));
	}
	
	public static Result logout() {
		Secured.removeUserSession(session());
		return redirect(routes.Application.index());
	}
}
