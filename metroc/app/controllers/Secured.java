package controllers;

import java.util.HashMap;

import models.User;

import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.Http.Session;

public class Secured extends Security.Authenticator {

	public static final String KEY = "mixture-key";
	public static final String LOGIN = "mixture-login";
	public static final String ROLE = "mixture-role";


	private static HashMap<String, String> keys = new HashMap<String, String>();
	private static HashMap<String, String> logins = new HashMap<String, String>();


	@Override
	public String getUsername(Context ctx) {
		String key = ctx.session().get(KEY);

		if (key == null)
			return null;

		String login = ctx.session().get(LOGIN);

		if (login == null)
			return null;

		String actualLogin = getUserSessionLogin(key);

		if (actualLogin == null)
			return null;

		if (!actualLogin.equals(login))
			return null;

		return login;
	}

	@Override
	public Result onUnauthorized(Context ctx) {
		return redirect(routes.Application.login());
	}

	public synchronized static void setUserSession(String key, String login) {
		String oldKey = keys.get(login);
		logins.remove(login);
		keys.remove(oldKey);
		keys.put(key, login);
		logins.put(login, key);
	}

	public synchronized static void removeUserSession(String key) {
		String login = keys.get(key);
		logins.remove(login);
		keys.remove(key);
	}
	
	public synchronized static String getLogin(Session session) {
		String key = session.get(KEY);
		return key == null ? null : keys.get(key);
	}

	public synchronized static boolean isAutorizedUser(String key, String login) {
		return login != null && key != null && keys.get(key) != null && keys.get(key).equals(login);
	}

	public synchronized static String getUserSessionLogin(String key) {
		return keys.get(key);
	}
	
	public static boolean isAuthorized(Session session) {
		String key = session.get(KEY);
		String login = session.get(LOGIN);
		return isAutorizedUser(key, login);
	}
	
	public static boolean isAdmin(Session session) {
		String role = session.get(ROLE);
		return isAuthorized(session) && role.equals(User.ROLE_ADMIN.toString());
	}

	public static boolean removeUserSession(Session session) {
		String key = session.get(KEY);
		session.clear();

		if (KEY != null)
			removeUserSession(key);

		return true;
	}

	public static boolean setUserSession(Session session, String login, String role) {
		String key = generateKey(login);
		setUserSession(key, login);

		session.put(KEY, key);
		session.put(LOGIN, login);
		session.put(ROLE, role.toString());
		return true;
	}

	public static String generateKey(String string) {
		return Helper.getMD5(string);
	}

}
