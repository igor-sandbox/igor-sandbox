package controllers;

import play.data.Form;
import models.User;

public class LoginUserForm {

	public String login;

	public String password;

	public static boolean validateForm(Form<LoginUserForm> form) {

		boolean isHaveErrors = false;

		if (Helper.addErrorIfEmptyField(form, "login",
				"Логин должен быть заполнен"))
			isHaveErrors = true;

		if (Helper.addErrorIfEmptyField(form, "password",
				"Пароль должен быть заполнен"))
			isHaveErrors = true;

		if (!isHaveErrors) {
			User user = User.findUserByLoginAndHash(form);
			if (user == null) {
				Helper.addFormError(form, "Такого пользователя не существует.");
				isHaveErrors = true;
			}
		}

		return isHaveErrors;
	}

}
