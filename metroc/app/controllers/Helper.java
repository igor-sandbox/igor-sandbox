package controllers;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import play.data.Form;
import play.data.validation.ValidationError;

public class Helper {

	public static String getMD5(String input) {
		String hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			hash = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}

	public static void addFormError(Form<?> form, String error) {
		addError(form, "form", error);
	}

	public static void addError(Form<?> form, String fieldName, String error) {
		List<ValidationError> errors = form.errors().get(fieldName);
		if (errors == null) {
			errors = new ArrayList<ValidationError>();
			form.errors().put(fieldName, errors);
		}
		errors.add(new ValidationError(fieldName, error, Collections
				.emptyList()));
	}

	public static String getValue(Form<?> form, String fieldName) {
		return form.field(fieldName).value();
	}

	public static boolean isEmpty(Form<?> form, String fieldName) {
		String value = getValue(form, fieldName);
		return value == null || value.trim().length() == 0;
	}

	public static boolean isEquals(Form<?> form, String fieldName1,
			String fieldName2) {
		String value1 = getValue(form, fieldName1);
		String value2 = getValue(form, fieldName1);
		return value1.equals(value2);
	}

	public static boolean addErrorIfEmptyField(Form<?> form, String fieldName,
			String error) {
		if (isEmpty(form, fieldName)) {
			addError(form, fieldName, error);
			return true;
		}
		return false;
	}

	public static boolean addErrorIfNotEqualsFields(Form<?> form,
			String fieldName1, String fieldName2, String error) {
		if (!isEquals(form, fieldName1, fieldName2)) {
			addError(form, fieldName2, error);
			return true;
		}
		return false;
	}

}
