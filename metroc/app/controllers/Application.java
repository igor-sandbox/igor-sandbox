package controllers;

import java.util.List;
import java.util.ArrayList;

import models.User;
import models.Category;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.login;
import views.html.registration;

public class Application extends Controller {

	// ============== pages =================//

	public static Result login() {
		return ok(login.render(form(LoginUserForm.class)));
	}

	public static Result registration() {
		return ok(registration.render(form(RegisterUserForm.class)));
	}
	
	public static Result index() {
		return ok(index.render());
	}

	// ============== handles ==================//

	public static List<Category> categories() {
		List<Category> categories = Category.all();
		return categories;
	}

	public static List<ArrayList<Category>> categories(int columns) {
		List<Category> categories = Category.all();
		int limit = categories.size()/columns;

		List<ArrayList<Category>> all = new ArrayList<ArrayList<Category>>(columns);
		for(int i=0; i<columns; i++) {
			all.add(new ArrayList<Category>());
		}
		for(int i=0; i<columns; i++) {
			for(int j = limit*i; j<limit*i+limit; j++) {
				all.get(i).add(categories.get(i));		
			}
		}
		return all;
	}

	public static Result authenticate() {
		Form<LoginUserForm> form = form(LoginUserForm.class).bindFromRequest();
		LoginUserForm.validateForm(form);
		if (form.hasErrors()) {
			return badRequest(login.render(form));
		}

		LoginUserForm objForm = form.get();
		User user = User.findByLogin(objForm.login);
		Secured.setUserSession(session(), objForm.login, user.role);
		return redirect(routes.InnerApp.profile());
	}

	public static Result addUser() {
		Form<RegisterUserForm> form = form(RegisterUserForm.class)
				.bindFromRequest();
		RegisterUserForm.validateForm(form);
		if (form.hasErrors()) {
			return badRequest(registration.render(form));
		}

		RegisterUserForm objForm = form.get();
		User.register(objForm);
		return redirect(routes.Application.index());
	}

}
