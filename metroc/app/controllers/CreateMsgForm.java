package controllers;

import play.data.Form;

public class CreateMsgForm {

	public String message;

	public static boolean validateForm(Form<CreateMsgForm> form) {

		boolean isHaveErrors = false;

		if (Helper.addErrorIfEmptyField(form, "message",
				"Сообщение не должно быть пустым"))
			isHaveErrors = true;
		
		return isHaveErrors;
	}

}
