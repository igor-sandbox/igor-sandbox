# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table category (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  constraint pk_category primary key (id))
;

create table file (
  id                        bigint auto_increment not null,
  msg_id                    bigint,
  name                      varchar(255),
  description               varchar(255),
  data                      varchar(255),
  constraint pk_file primary key (id))
;

create table message (
  id                        bigint auto_increment not null,
  owner_id                  bigint,
  message                   varchar(255),
  type                      varchar(255),
  role                      varchar(255),
  subcategory_id            bigint,
  contacts                  varchar(255),
  status                    varchar(255),
  date                      varchar(255),
  assigner_id               bigint,
  constraint pk_message primary key (id))
;

create table subcategory (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  category_id               bigint,
  constraint pk_subcategory primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  role                      varchar(255),
  login                     varchar(255),
  hash                      varchar(255),
  email                     varchar(255),
  name                      varchar(255),
  sname                     varchar(255),
  cname                     varchar(255),
  type                      varchar(255),
  description               varchar(255),
  region                    varchar(255),
  address                   varchar(255),
  constraint pk_user primary key (id))
;


create table user_subcategory (
  user_id                        bigint not null,
  subcategory_id                 bigint not null,
  constraint pk_user_subcategory primary key (user_id, subcategory_id))
;
alter table file add constraint fk_file_msg_1 foreign key (msg_id) references message (id) on delete restrict on update restrict;
create index ix_file_msg_1 on file (msg_id);
alter table message add constraint fk_message_owner_2 foreign key (owner_id) references user (id) on delete restrict on update restrict;
create index ix_message_owner_2 on message (owner_id);
alter table message add constraint fk_message_subcategory_3 foreign key (subcategory_id) references subcategory (id) on delete restrict on update restrict;
create index ix_message_subcategory_3 on message (subcategory_id);
alter table message add constraint fk_message_assigner_4 foreign key (assigner_id) references user (id) on delete restrict on update restrict;
create index ix_message_assigner_4 on message (assigner_id);
alter table subcategory add constraint fk_subcategory_category_5 foreign key (category_id) references category (id) on delete restrict on update restrict;
create index ix_subcategory_category_5 on subcategory (category_id);



alter table user_subcategory add constraint fk_user_subcategory_user_01 foreign key (user_id) references user (id) on delete restrict on update restrict;

alter table user_subcategory add constraint fk_user_subcategory_subcatego_02 foreign key (subcategory_id) references subcategory (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table category;

drop table file;

drop table message;

drop table subcategory;

drop table user_subcategory;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

