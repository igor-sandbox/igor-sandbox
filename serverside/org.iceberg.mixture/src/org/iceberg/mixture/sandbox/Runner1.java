package org.iceberg.mixture.sandbox;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.iceberg.mixture.core.BuisnessLogic;
import org.iceberg.mixture.db.HibernateUtil;
import org.iceberg.mixture.db.dao.DAOAbstractFactory;
import org.iceberg.mixture.utils.Config;
import org.iceberg.mixture.ws.WebService;

public class Runner1 {
	
    private static final Logger log = Logger.getLogger(Runner1.class);

    public static void main(String[] args) throws ConfigurationException {

        // init config
    	log.trace("Initializing configuration...");
        Config.init();
        
        log.trace("Initializing hibernate utils...");
        HibernateUtil.init();
        
        log.trace("Initializing database factory...");
        DAOAbstractFactory.init();
        
        // init Task loic
        log.trace("Initializing buisness logic...");
        BuisnessLogic.init();
        
        // init web service
        log.trace("Creating web service...");
        WebService ws = new WebService();
        
        // starting web service
        log.trace("Starting web service...");
        ws.publish();
        
        log.trace("Ok");
    }

}
