package org.iceberg.mixture.db;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory object.
 *
 * @author iceberg
 */
public class HibernateUtil {
	
    private static final Logger         log = Logger.getLogger(HibernateUtil.class);
    private static SessionFactory sessionFactory;

    public static void init() {
        try {

            // Create the SessionFactory from standard (hibernate.cfg.xml)
            // config file.
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {

            // Log the exception.
            System.err.println("Initial SessionFactory creation failed." + ex);

            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static List executeHQLQuery(String hql) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();

            Query q          = session.createQuery(hql);
            List  resultList = q.list();

            session.getTransaction().commit();

            return resultList;
        } catch (HibernateException he) {
            log.error("Error during execute HQL!", he);
        }

        return null;
    }
}
