package org.iceberg.mixture.db.pojo;

public class Users  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer role;
    private String login;
    private String email;
    private String hash;
     
    public Users() {
    }

    public Users(String login, String hash) {
        this.login = login;
        this.hash = hash;    	
    }
	
    public Users(Integer role, String login, String email, String hash) {
        this.role = role;
        this.login = login;
        this.hash = hash;
        this.email = email;
    }

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getRole() {
        return this.role;
    }
    
    public void setRole(Integer role) {
        this.role = role;
    }
    public String getLogin() {
        return this.login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }
    public String getHash() {
        return this.hash;
    }
    
    public void setHash(String hash) {
        this.hash = hash;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String mail) {
        this.email = mail;
    }
}

