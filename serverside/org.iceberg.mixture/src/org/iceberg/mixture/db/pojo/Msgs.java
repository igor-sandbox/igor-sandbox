package org.iceberg.mixture.db.pojo;

public class Msgs {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Integer owner;
    private String msg;
     
    public Msgs() {
    }

    public Msgs(Integer owner, String msg) {
        this.owner = owner;
        this.msg = msg;
    }
    
    public Msgs(Integer id, Integer owner, String msg) {
    	this.id = id;
        this.owner = owner;
        this.msg = msg;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
