package org.iceberg.mixture.db.dao;

import org.iceberg.mixture.db.dao.intf.DAOMsgsIF;
import org.iceberg.mixture.db.dao.intf.DAOUsersIF;
import org.iceberg.mixture.utils.Config;


/**
 *
 * @author iceberg
 */
public abstract class DAOAbstractFactory {
	
    protected static DAOAbstractFactory instance = null;
    protected static DAOUsersIF        daoUsers;
    protected static DAOMsgsIF        daoMsgs;

    public static enum Type { ST_DB_MYSQL }


    public static synchronized DAOAbstractFactory getDAOFactory() {
        if (instance == null)
            init();
        return instance;
    }

    public static synchronized void setFactoryType(Type type) {
        DAOAbstractFactory.getInstance(type);
    }

    public static void init() {
        getInstance(Config.getDBType());
    }

    private static synchronized DAOAbstractFactory getInstance(Type type) {
        if (instance == null) {
            switch (type) {
            case ST_DB_MYSQL :
                instance = new DAOFactoryMySQL().getInstance();

                break;

            default :
                instance = new DAOFactoryMySQL().getInstance();
            }
        }

        return instance;
    }

    public abstract DAOUsersIF getDAOUsers();
    public abstract DAOMsgsIF getDAOMsgs();
}

