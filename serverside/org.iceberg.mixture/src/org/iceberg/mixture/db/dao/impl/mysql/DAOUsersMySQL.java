package org.iceberg.mixture.db.dao.impl.mysql;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.iceberg.mixture.db.HibernateUtil;
import org.iceberg.mixture.db.dao.impl.DAOGeneric;
import org.iceberg.mixture.db.dao.intf.DAOUsersIF;
import org.iceberg.mixture.db.pojo.Users;

/**
 * 
 * @author iceberg
 */
public class DAOUsersMySQL extends DAOGeneric<Users, Integer> implements
		DAOUsersIF {

	public Users saveOrReturnExists(Users obj, Session session) {
		Users retObj = null;

		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("login", obj.getLogin()));
			criteria.add(Restrictions.eq("role", obj.getRole()));
			criteria.add(Restrictions.eq("hash", obj.getHash()));
			criteria.add(Restrictions.eq("email", obj.getEmail()));
			List<Users> objects = (List<Users>) criteria.list();
			if (objects != null && objects.size() > 0) {
				retObj = objects.get(0);
			} else {
				session.save(obj);
				retObj = obj;
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Save operation failed!", e);
		}
		return retObj;
	}

	public Users saveOrReturnExists(Users obj) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Users retObj = saveOrReturnExists(obj, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	public Users returnExists(Users obj) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Users retObj = returnExists(obj, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	public Users returnExists(Users obj, Session session) {
		Users retObj = null;

		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("login", obj.getLogin()));
			criteria.add(Restrictions.eq("hash", obj.getHash()));
			List<Users> objects = (List<Users>) criteria.list();
			if (objects != null && objects.size() > 0) {
				retObj = objects.get(0);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Save operation failed!", e);
		}
		return retObj;
	}

	@Override
	public boolean loginExists(Users user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean retObj = loginExists(user, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	@Override
	public boolean emailExists(Users user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean retObj = emailExists(user, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	@Override
	public boolean hashExists(Users user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean retObj = loginExists(user, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	@Override
	public boolean loginExists(Users user, Session session) {
		boolean result = false;
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("login", user.getLogin()));
			List<Users> objects = (List<Users>) criteria.list();
			if (objects != null && objects.size() > 0) {
				result = true;
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Save operation failed!", e);
		}
		return result;
	}

	@Override
	public boolean emailExists(Users user, Session session) {
		boolean result = false;
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("email", user.getLogin()));
			List<Users> objects = (List<Users>) criteria.list();
			if (objects != null && objects.size() > 0) {
				result = true;
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Save operation failed!", e);
		}
		return result;
	}

	@Override
	public boolean hashExists(Users user, Session session) {
		boolean result = false;
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("hash", user.getLogin()));
			List<Users> objects = (List<Users>) criteria.list();
			if (objects != null && objects.size() > 0) {
				result = true;
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Save operation failed!", e);
		}
		return result;
	}

	@Override
	public Users returnExists(String login) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Users retObj = returnExists(login, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	@Override
	public Users returnExists(String login, Session session) {
		Users result = null;
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("login", login));
			List<Users> objects = (List<Users>) criteria.list();
			if (objects != null && objects.size() > 0) {
				result = objects.get(0);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Return user by login operation failed!", e);
		}
		return result;
	}

}
