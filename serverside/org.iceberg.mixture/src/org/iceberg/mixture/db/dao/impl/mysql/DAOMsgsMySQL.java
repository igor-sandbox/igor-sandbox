package org.iceberg.mixture.db.dao.impl.mysql;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.iceberg.mixture.db.HibernateUtil;
import org.iceberg.mixture.db.dao.impl.DAOGeneric;
import org.iceberg.mixture.db.dao.intf.DAOMsgsIF;
import org.iceberg.mixture.db.pojo.Msgs;
import org.iceberg.mixture.db.pojo.Users;

public class DAOMsgsMySQL extends DAOGeneric<Users, Integer> implements DAOMsgsIF {
	
	public Msgs saveOrReturnExists(Users user, String msg, Session session) {
		Msgs retObj = new Msgs(user.getId(), msg);
		try {
			session.beginTransaction();
			session.save(retObj);
			session.refresh(retObj);
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Save operation failed!", e);
		}
		return retObj;
	}

	public Msgs saveOrReturnExists(Users user, String msg) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Msgs retObj = saveOrReturnExists(user, msg, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	@Override
	public List<Msgs> returnMsgs(Users user, Integer index, Integer count) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Msgs> retObj = returnMsgs(user, index, count, session);
		if ((session != null) && session.isOpen()) {
			session.close();
		}
		return retObj;
	}

	@Override
	public List<Msgs> returnMsgs(Users user, Integer index, Integer count,
			Session session) {
		List<Msgs> msgs = null;
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Msgs.class);
			criteria.add(Restrictions.eq("owner", user.getId()));
			msgs = criteria.list();
			
			/*ScrollableResults scrollableResults = criteria.scroll();
			
			msgs = new ArrayList<Msgs>();
			
			while(scrollableResults.scroll(index + 1)) {
				for(int i=0; i<count; i++) {
					Msgs msg = (Msgs)scrollableResults.get;
					if(msg == null) {
						break;
					}
					msgs.add(msg);
				}
			}*/
			session.getTransaction().commit();
		} catch (Exception e) {
			log.error("Return messages operation failed!", e);
		}
		return msgs;
	}
	

}
