package org.iceberg.mixture.db.dao.intf;

import java.util.List;

import org.hibernate.Session;
import org.iceberg.mixture.db.pojo.Msgs;
import org.iceberg.mixture.db.pojo.Users;

public interface DAOMsgsIF extends DAOGenericIF<Users, Integer> {

	Msgs saveOrReturnExists(Users user, String msg);
	Msgs saveOrReturnExists(Users user, String msg, Session session);
	List<Msgs> returnMsgs(Users user, Integer index, Integer count);
	List<Msgs> returnMsgs(Users user, Integer index, Integer count, Session session);

}
