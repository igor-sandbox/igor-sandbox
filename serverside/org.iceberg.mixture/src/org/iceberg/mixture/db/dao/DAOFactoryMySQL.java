
package org.iceberg.mixture.db.dao;

import org.iceberg.mixture.db.dao.impl.mysql.DAOMsgsMySQL;
import org.iceberg.mixture.db.dao.impl.mysql.DAOUsersMySQL;
import org.iceberg.mixture.db.dao.intf.DAOMsgsIF;
import org.iceberg.mixture.db.dao.intf.DAOUsersIF;



/**
 *
 * @author iceberg
 */
public class DAOFactoryMySQL extends DAOAbstractFactory {
	
    public synchronized DAOAbstractFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactoryMySQL();
        }

        return instance;
    }

    @Override
    public DAOUsersIF getDAOUsers() {
        if (daoUsers == null) {
            daoUsers = new DAOUsersMySQL();
        }

        return daoUsers;
    }
    
    @Override
    public DAOMsgsIF getDAOMsgs() {
        if (daoMsgs == null) {
            daoMsgs = new DAOMsgsMySQL();
        }

        return daoMsgs;
    }

}
