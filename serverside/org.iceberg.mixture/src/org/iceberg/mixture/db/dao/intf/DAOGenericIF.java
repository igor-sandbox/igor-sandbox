package org.iceberg.mixture.db.dao.intf;

import java.io.Serializable;
import org.hibernate.Session;

/**
 *
 * @author iceberg
 */
public interface DAOGenericIF<T extends Serializable, PK extends Serializable> {
    PK save(T newInstance);

    T read(PK id);

    void delete(T transientObject);

    void update(T newInstance);

    void saveOrUpdate(T newInstance);


    // on openned sessions
    PK save(T newInstance, Session session);

    T read(PK id, Session session);

    void delete(T transientObject, Session session);

    void update(T newInstance, Session session);

    void saveOrUpdate(T newInstance, Session session);
}
