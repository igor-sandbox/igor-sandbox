package org.iceberg.mixture.db.dao.intf;

import org.hibernate.Session;
import org.iceberg.mixture.db.pojo.Users;


/**
 *
 * @author iceberg
 */
public interface DAOUsersIF extends DAOGenericIF<Users, Integer> {

    public Users saveOrReturnExists(Users obj);
    public Users saveOrReturnExists(Users obj, Session session);

    public Users returnExists(String login);
    public Users returnExists(String login, Session session);
    
    public Users returnExists(Users obj);
    public Users returnExists(Users obj, Session session);
    
	public boolean loginExists(Users user);
	public boolean loginExists(Users user,  Session session);
	
	public boolean emailExists(Users user);
	public boolean emailExists(Users user,  Session session);
	
	public boolean hashExists(Users user);
	public boolean hashExists(Users user,  Session session);

}