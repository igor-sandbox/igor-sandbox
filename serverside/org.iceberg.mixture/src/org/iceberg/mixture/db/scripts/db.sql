drop table if exists msgs;
drop table if exists users;

create table if not exists users(
	id int(10) unsigned not null auto_increment,
	role int(10) not null,
	login varchar(50) not null,
	hash varchar(255) not null,
	email varchar(255) not null,

     UNIQUE (login),
     UNIQUE (email),
	primary key(id)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

create table if not exists msgs(
	id int(10) unsigned not null auto_increment,
	owner int(10) unsigned not null,
	msg text not null,
	primary key(id)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

alter table msgs add constraint foreign key (owner) references users(id);

