package org.iceberg.mixture.ws;

import javax.jws.WebMethod;
import javax.xml.ws.Endpoint;

import org.apache.log4j.Logger;
import org.iceberg.mixture.core.BSInfo;
import org.iceberg.mixture.core.BSInfoInner;
import org.iceberg.mixture.core.BSInfoMsgs;
import org.iceberg.mixture.core.BuisnessLogic;
import org.iceberg.mixture.utils.Config;

/**
 * 
 * @author iceberg
 */
@javax.jws.WebService
public class WebService {

	private static final Logger log = Logger.getLogger(WebService.class);

	public void publish() {
		log.info("Starting Mixture service with addr \"" + Config.getWSDLAddr()
				+ "\"...");
		Endpoint.publish(Config.getWSDLAddr() + "?wsdl", this);
		log.info("Mixture service successfully started");
	}

	@WebMethod
	public BSInfo addUser(String login, String email, String password,
			String repassword) {
		log.info("\"Create user\" web method called with parameters: login: \""
				+ login + "\", email: \"" + email + "\"," + " password: "
				+ password + ", repassword: " + repassword);
		return BuisnessLogic.addUser(login, email, password, repassword);
	}

	@WebMethod
	public BSInfoInner loginUser(String login, String password) {
		return BuisnessLogic.loginUser(login, password);
	}

	@WebMethod
	public BSInfo logoutUser(String key) {
		return BuisnessLogic.logoutUser(key);
	}

	@WebMethod
	public BSInfo addMsg(String key, String msg) {
		return BuisnessLogic.addMsg(key, msg);
	}
	
	@WebMethod
	public BSInfoMsgs getMsgs(String key, Integer index, Integer count) {
		return BuisnessLogic.getMsgs(key, index, count);
	}
	


}
