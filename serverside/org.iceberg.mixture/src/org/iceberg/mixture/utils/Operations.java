package org.iceberg.mixture.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.iceberg.mixture.db.pojo.Users;

public class Operations {
	
	private static Random random = new Random();
	
    public static int byteArrayToInt(byte[] b, int offset) {
        int value = 0;

        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;

            value += (b[i + offset] & 0x000000FF) << shift;
        }

        if (value < 0) {
            return value + Integer.MAX_VALUE;
        }

        return value;
    }

    public static String getMd5Crc(String login_plus_pass) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");

        digest.update(login_plus_pass.getBytes());

        return String.valueOf(byteArrayToInt(digest.digest(), 12));
    }

    
	public static String getKey(Users user) {
		int randomValue1 = Math.abs(random.nextInt()) % 9;
		int randomValue2 = Math.abs(random.nextInt()) % 9;
		String key = user.getLogin() + "MIX" + randomValue1 + user.getHash() + randomValue2;
		return key;
	}

}
