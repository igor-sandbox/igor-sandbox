package org.iceberg.mixture.utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Logger;
import org.iceberg.mixture.db.dao.DAOAbstractFactory;


public class Config {
    
    private static final Logger log = Logger.getLogger(Config.class);
    private static String              configFile = "src/config.xml";
    private static String              wsdlAddr;

    private static String              wsdlPort;
    private static String              wsdlHost;
    private static String              wsdlName;
    
    private static String hibernateConfig;

    public static DAOAbstractFactory.Type getDBType() {
        return DAOAbstractFactory.Type.ST_DB_MYSQL;
    }

    private Config() {}

    public static synchronized void save(final String filename) throws ConfigurationException {
        configFile = filename;
        save();
    }

    public static synchronized void init(final String filename) throws ConfigurationException {
        configFile = filename;
        init();
    }

    public static synchronized void init() throws ConfigurationException {
        log.trace("Loading config...");
        XMLConfiguration config = new XMLConfiguration(configFile);

        hibernateConfig = config.getString("db");

        wsdlHost = config.getString("web-service-config.host");
        wsdlPort = config.getString("web-service-config.port");
        wsdlName = config.getString("web-service-config.name");


        wsdlAddr = "http://"+wsdlHost+':'+wsdlPort+'/'+wsdlName;
    }

    public synchronized static void save() throws ConfigurationException {
        XMLConfiguration config = new XMLConfiguration();

        config.setEncoding("UTF-8");
        config.setFileName(configFile);
        config.setRootElementName("ldvo-configuration");

        config.addProperty("web-service-config.host", wsdlHost);
        config.addProperty("web-service-config.port", wsdlPort);
        config.addProperty("web-service-config.name", wsdlName);

        config.addProperty("db", hibernateConfig);

        config.save();
    }

    public static synchronized String getWSDLAddr() {
        return wsdlAddr;
    }

}
