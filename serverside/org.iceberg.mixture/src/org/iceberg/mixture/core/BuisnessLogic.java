package org.iceberg.mixture.core;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.iceberg.mixture.db.dao.DAOAbstractFactory;
import org.iceberg.mixture.db.pojo.Msgs;
import org.iceberg.mixture.db.pojo.Users;
import org.iceberg.mixture.utils.Operations;

/**
 * 
 * @author iceberg
 */
public class BuisnessLogic {

	public static final int ROLE_USER = 0;

	public static final String STATUS_OK = "OK";
	public static final String STATUS_FAILED = "FAILED";
	public static final String STATUS_NOT_LOGGED_IN = "NOT_LOGGED_IN";

	private static final Logger log = Logger.getLogger(BuisnessLogic.class);
	private static DAOAbstractFactory dao;

	private static Map<String, String> keys = new HashMap<String, String>();
	private static Map<String, String> logins = new HashMap<String, String>();

	/**
	 * 
	 * Метод возвращает пользователя, если залогинен,<br>
	 * если метод вернул null, значит считаем, что пользователь не<br>
	 * залогинился.
	 * 
	 * @param key
	 * @return
	 */
	public static Users getLoggedInUser(String key) {
		log.trace("getLoggedUser method called.");
		if (key == null) {
			log.trace("key is null.");
			return null;
		}

		if (!isLoggedInKey(key)) {
			log.trace("Key not logged in.");
			return null;
		}

		String login = getLoginByKey(key);
		if (login == null) {
			log.trace("Cann't get login for key");
			return null;
		}

		return dao.getDAOUsers().returnExists(login);
	}

	public static BSInfoMsgs getMsgs(String key, Integer index, Integer count) {
		log.trace("getMsgs method called.");
		BSInfoMsgs bsinfo = new BSInfoMsgs();

		log.trace("Standart null checks");
		if (key == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не введен ключ.";
			return bsinfo;
		}
		if (index == null || index < 0) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не верный индекс.";
			return bsinfo;
		}
		if (count == null || count < 1) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не верное количество.";
			return bsinfo;
		}

		Users user = getLoggedInUser(key);
		if (user == null) {
			bsinfo.status = STATUS_NOT_LOGGED_IN;
			bsinfo.descr = "Пользователь не залогинился.";
			return bsinfo;
		}

		log.trace("Try to get messages from DB");
		List<Msgs> msgs = dao.getDAOMsgs().returnMsgs(user, index, count);
		if (msgs == null) {
			log.trace("Failed getting messages");
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не могу вернуть сообщения.";
			return bsinfo;
		}

		log.trace("Messages retrived and now combined into BSInfo");
		bsinfo.msgs = new String[msgs.size()];
		for (int i = 0; i < msgs.size(); i++) {
			bsinfo.msgs[i] = msgs.get(i).getMsg();
		}

		bsinfo.status = STATUS_OK;
		bsinfo.descr = STATUS_OK;
		return bsinfo;
	}

	public static BSInfo addMsg(String key, String msg) {
		log.trace("addMsg method called.");
		BSInfo bsinfo = new BSInfo();

		log.trace("Standart null checks");
		if (key == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не введен ключ.";
			return bsinfo;
		}
		if (msg == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Пустое сообщение.";
			return bsinfo;
		}

		Users user = getLoggedInUser(key);
		if (user == null) {
			bsinfo.status = STATUS_NOT_LOGGED_IN;
			bsinfo.descr = "Пользователь не залогинился.";
			return bsinfo;
		}

		log.trace("Try to save message to DB");
		Msgs omsg = dao.getDAOMsgs().saveOrReturnExists(user, msg);
		if (omsg == null) {
			log.trace("Failed saving message to DB");
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не могу записать сообщение.";
			return bsinfo;
		}

		bsinfo.status = STATUS_OK;
		bsinfo.descr = STATUS_OK;
		return bsinfo;
	}

	public static BSInfo addUser(String login, String email, String password,
			String repassword) {
		log.trace("addUser method called.");

		BSInfo bsinfo = new BSInfo();

		if (login == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не введен логин.";
			return bsinfo;
		}
		if (email == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Электронная почта не введена.";
			return bsinfo;
		}
		if (password == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Пароль не введен.";
			return bsinfo;
		}
		if (repassword == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Повторный пароль не введен.";
			return bsinfo;
		}
		if (!password.equals(repassword)) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Пароли не совпадают.";
			return bsinfo;
		}

		Users user = null;
		try {
			user = new Users(ROLE_USER, login, email,
					Operations.getMd5Crc(login + password));
		} catch (NoSuchAlgorithmException ex) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Проблема на сервере.";
			return bsinfo;
		}

		if (dao.getDAOUsers().loginExists(user)) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Пользователь с таким логином уже зарегистрирован.";
			return bsinfo;
		}
		if (dao.getDAOUsers().emailExists(user)) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Пользователь с такой почтой уже зарегистрирован.";
			return bsinfo;
		}
		if (dao.getDAOUsers().hashExists(user)) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Выберите, пожалуйста, другой логин.";
			return bsinfo;
		}

		user = dao.getDAOUsers().saveOrReturnExists(user);

		bsinfo.status = STATUS_OK;
		bsinfo.descr = STATUS_OK;
		return bsinfo;
	}

	public static void init() {
		dao = DAOAbstractFactory.getDAOFactory();
	}

	public static BSInfo logoutUser(String key) {
		BSInfoInner bsinfo = new BSInfoInner();
		if (key == null || key.trim().length() == 0) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Ключ слишком короткий.";
			return bsinfo;
		}

		bsinfo.status = STATUS_OK;
		if (isLoggedInKey(key)) {
			logoutByKey(key);
			bsinfo.descr = STATUS_OK;
		} else {
			bsinfo.descr = "Пользователь не был залогинен.";
		}

		return bsinfo;
	}

	public static BSInfoInner loginUser(String login, String password) {
		log.trace("loginUser method called.");

		BSInfoInner bsinfo = new BSInfoInner();

		if (login == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не введен логин.";
			return bsinfo;
		}
		if (password == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Не введен пароль.";
			return bsinfo;
		}

		Users user = null;
		try {
			user = new Users(login, Operations.getMd5Crc(login + password));
		} catch (NoSuchAlgorithmException ex) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Проблема на сервере.";
			return bsinfo;
		}

		user = dao.getDAOUsers().returnExists(user);

		if (user == null) {
			bsinfo.status = STATUS_FAILED;
			bsinfo.descr = "Такого пользователя не существует.";
			return bsinfo;
		}

		if (isLoggedInLogin(user.getLogin())) {
			logoutByLogin(user.getLogin());
		}

		// Генерируем ключ для последующего доступа без логина
		bsinfo = new BSInfoInner();

		bsinfo.key = Operations.getKey(user);
		bsinfo.status = STATUS_OK;
		bsinfo.descr = STATUS_OK;

		setLoggedIn(user.getLogin(), bsinfo.key);

		return bsinfo;
	}

	public static synchronized void setLoggedIn(String login, String key) {
		keys.put(login, key);
		keys.put(key, login);
	}

	public static synchronized boolean isLoggedIn(String login, String key) {
		return keys.get(login) == key;
	}

	public static synchronized boolean isLoggedInLogin(String login) {
		return logins.containsKey(login);
	}

	public static synchronized boolean isLoggedInKey(String key) {
		return keys.containsKey(key);
	}

	public static synchronized String getLoginByKey(String key) {
		return keys.get(key);
	}

	public static synchronized String getKeyByLogin(String login) {
		return logins.get(login);
	}

	public static synchronized void logoutByKey(String key) {
		String login = keys.get(key);
		keys.remove(key);
		logins.remove(login);
	}

	public static synchronized void logoutByLogin(String login) {
		String key = logins.get(login);
		keys.remove(key);
		logins.remove(login);
	}

}
