#!/usr/bin/perl -w

$ENV{'WSDLADDR'} = "http://localhost:9888/ldvo";

use SOAP::Lite;

my $csd = SOAP::Lite -> service($ENV{'WSDLADDR'}.'?wsdl');


my @rules = (
	SOAP::Data-> type ("string") -> name ("item") -> value ("ARRAY esponse_one 1"),
	SOAP::Data-> type ("string") -> name ("item") -> value ("ARRAY esponse_one 2")
);

my $command = $csd->sandBox(SOAP::Data-> type ("string") -> name ("arg0") -> value ("response_one"),
			    @rules);

if($command->{status} eq 'OK') {
	print $command->{id}."\n";
	print $command->{desc}."\n";
} else {
	print $command->{desc}."\n";
}
