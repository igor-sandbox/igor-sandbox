<?php

define("STATUS_OK", "OK");
define("STATUS_FAILED", "FAILED");

$wsAddr = null;
$wsClient = null;

ini_set("soap.wsdl_cache_enabled", "0");

function wsInit() {
	global $wsAddr;
	global $wsClient;
	if($wsAddr == null) {
		$wsAddr = "http://localhost:9888/ldvo?wsdl";
	}
        $wsClient = new SoapClient($wsAddr);
        if(!$wsClient) {
                $response['status'] = STATUS_FAILED;
                $response['descr'] = 'Cann\'t create SOAP Client.';
                return $response;
        }
	
        $response['status'] = STATUS_OK;
        $response['descr'] = STATUS_OK;
        return $response;
}

function wsGetClient() {
	global $wsClient;
	if($wsClient == null) {
		wsInit();
	}
	return $wsClient;
}

function wsAddUser($login, $email, $password, $repassword) {
       try {
		$wsargs['arg0'] = $login; 
		$wsargs['arg1'] = $email; 
		$wsargs['arg2'] = $password; 
		$wsargs['arg3'] = $repassword; 

                $client = wsGetClient();
                $wsresponse = $client->addUser($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;
		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}

function wsLogin($login, $pass) {
       try {
		$wsargs['arg0'] = $login;
		$wsargs['arg1'] = $pass;

                $client = wsGetClient();
                $wsresponse = $client->loginUser($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;
		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}



$response = wsLogin("iceberg", "mixture1604");
print("Operation finished with results:\n");
print("\tStatus     : ".$response['status']."\n");
print("\tDescription: ".$response['descr']."\n");


?>
