<?php
require_once("base.php");

$login=request_var('login','');
$password=request_var('password','');

$form = objLoginForm();

$status = "OK";
if(empty($login) || $login == "") {
	$form = errorToForm($form, "login", "Не заполнено поле login.");	
	$status = "ERROR";
} else {
	$form = fillInputToForm($form, "login", $login); 
}


if(empty($password) || $password == "") {
	$form = errorToForm($form, "password", "Вы не указали пароль.");	
	$status = "ERROR";
} 

$content = "";
if($status == "OK") {
	$response = wsLoginUser($login, $password);
	if($response['status'] == STATUS_OK) {
		$content .= getLoginSuccessContent();
	} else if($response['status'] == STATUS_FAILED) {
		$form = errorToFormMain($form, $response['descr']);	
		$content .= getLoginContent($form);
	}
} else {
	$content .= getLoginContent($form);
}
print($content);
?>

