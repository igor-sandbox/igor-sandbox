<?php
require_once("base/utils.php");
require_once("base/wsapi.php");
require_once("base/html.php");
require_once("base/container.php");
require_once("base/navbar.php");
require_once("base/forms.php");


function getPrefix() {
	return "index.php?action=";
}


function wrapLocalContent($content) {
	return "<div>\n$content</div>\n";
}

function getMenu() {
        $menu = array(
		"pmain" => "Главная",
		"pregistration" => "Регистрация",
		"paddmsg" => "Сообщение",
		"pmsgs" => "Просмотр сообщений",
		"plogin" => "Войти",
		"plogout" => "Выход",
		"pabout" => "О нас",
	);  
	return $menu;
}

function getPageContent($prefix, $menu, $active, $content) {
	$navbar = navbar($prefix, $menu, $active);
	$container = container($navbar.$content);
	return wrapContent($container);
}

function getPageLocal($active, $content) {
	$menu = getMenu();
	$prefix = getPrefix();
	return getPageContent($prefix, $menu, $active, $content);
}

function getLoginContent($form) {
	if(empty($form)) {
		$form = objLoginForm();
	}
	$content = form($form);
	$content .= "<script src=\"javascripts/login.js\" type=\"text/javascript\"></script>\n";
	return wrapLocalContent($content);
}

function getRegistrationContent($form) {
	if(empty($form)) {
		$form = objRegForm();
	}
	$content = form($form);
	$content .= "<script src=\"javascripts/reg.js\" type=\"text/javascript\"></script>\n";
	return wrapLocalContent($content);
}

function getAddMsgContent($form) {
	if(empty($form)) {
		$form = objAddMsgForm();
	}
	$content = form($form);
	$content .= "<script src=\"javascripts/addmsg.js\" type=\"text/javascript\"></script>\n";
	return wrapLocalContent($content);
}

function getLoginSuccessContent() {
	$content = "Добро пожаловать в сервис!";
	return wrapLocalContent($content);
}

function getMsgsContent() {
	$result = wsGetMsgs(0, 10); 
	$content = "";
	if($result['status'] == STATUS_OK) {

$content .=<<<HTML
<table class="table table-striped">
	<caption>Сообщения пользователей</caption>
	<thead>
		<tr>
			<th>№</th>
			<th>Сообщение</th>
		</tr>
	</thead>
	<tbody>
HTML;
		foreach ($result['msgs'] as $id => $msg) {
$content .=<<<HTML
		<tr>	
			<td>$id</td>
			<td>$msg</td>
		</tr>
HTML;
		}

$content .=<<<HTML
	</tbody>
</table>
HTML;
	} else if($result['status'] == STATUS_NOT_LOGGED_IN) {
		$content .= getLoginContent();
	} else {
		$content .= "Неизвестная ошибка";
	}
	return wrapLocalContent($content);
}

function getLogoutContent() {
	wsLogoutUser();
	$content = "Выход был выполнен успешно";
	return wrapLocalContent($content);
}

function getRegistrationSuccessContent() {
	$content = "Поздравляем, вы успешно зарегистрировались!";
	return wrapLocalContent($content);
}

function getAddMsgSuccessContent() {
	$content = "Поздравляем, ваше сообщение было успешно отправлено!";
	return wrapLocalContent($content);
}


function getRegistrationPageContent() {
	$active = "pregistration";
	$content = getRegistrationContent();
	return getPageLocal($active, $content);
}

function getMainPageContent() {
	$active = "pmain";
	$content = "Добро пожаловать на главную страницу сервиса!";
	return getPageLocal($active, $content);
}

function getLoginPageContent() {
	$active = "plogin";
	$content = getLoginContent();
	return getPageLocal($active, $content);
}

function getLogoutPageContent() {
	$active = "plogout";
	$content = getLogoutContent();
	return getPageLocal($active, $content);
}

function getAddMsgPageContent() {
	$active = "paddmsg";
	$content = getAddMsgContent();
	return getPageLocal($active, $content);
}

function getMsgsPageContent() {
	$active = "pmsgs";
	$content = getMsgsContent();
	return getPageLocal($active, $content);
}



?>
