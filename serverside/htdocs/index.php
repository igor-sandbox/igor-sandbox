<?php
require_once("base.php");

$action=request_var('action','');
$exit=false;

$content="";

if ($action == "pregistration" && !$exit) {
	$content=getRegistrationPageContent();

} else if ($action == "paddmsg" && !$exit) {
	$content=getAddMsgPageContent();

} else if ($action == "plogin" && !$exit) {
	$content=getLoginPageContent();

} else if ($action == "plogout" && !$exit) {
	$content=getLogoutPageContent();

} else if ($action == "pmsgs" && !$exit) {
	$content=getMsgsPageContent();

} else {
	$content=getMainPageContent();
}
print($content);
?>

