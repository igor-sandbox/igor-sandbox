<?php
require_once("base.php");

$login=request_var('login','');
$email=request_var('email','');
$password=request_var('password','');
$repassword=request_var('repassword','');

$form = objRegForm();


$status = "OK";
if(empty($login) || $login == "") {
	$form = errorToForm($form, "login", "Не заполнено поле login.");	
	$status = "ERROR";
} else {
	$form = fillInputToForm($form, "login", $login); 
}

if(empty($email) || $email == "") {
	$form = errorToForm($form, "email", "Вы не указали электронную почту.");	
	$status = "ERROR";
} else {
	$form = fillInputToForm($form, "email", $email); 
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$form = errorToForm($form, "email", "Не верный формат электронной почты.");	
		$status = "ERROR";
	}
}


if(empty($password) || $password == "") {
	$form = errorToForm($form, "password", "Вы не указали пароль.");	
	$status = "ERROR";
} 

if(strlen($password) < 9) {
	$form = errorToForm($form, "password", "Длина пароля должна быть больше восьми символов.");	
	$status = "ERROR";
}

if(empty($repassword) || $repassword == "") {
	$form = errorToForm($form, "repassword", "Поле для повторного ввода пароля не заполнено.");	
	$status = "ERROR";
}

if(!empty($repassword) && !empty($password) && $password != $repassword) {
	$form = errorToForm($form, "repassword", "Проли должны совпадать.");	
	$status = "ERROR";
}

$content = "";
if($status == "OK") {
	$response = wsAddUser($login, $email, $password, $repassword);
	if($response['status'] == STATUS_OK) {
		$content .= getRegistrationSuccessContent();
	} else if($response['status'] == STATUS_FAILED) {
		$form = errorToFormMain($form, $response['descr']);	
		$content .= getRegistrationContent($form);
	}
} else {
	$content .= getRegistrationContent($form);
}

print($content);
?>

