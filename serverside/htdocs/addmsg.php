<?php
require_once("base.php");

$msg=request_var('msg','');

$form = objAddMsgForm();

$status = "OK";
if(empty($msg) || $msg == "") {
	$form = errorToForm($form, "msg", "Сообщение не может быть пустым.");	
	$status = "ERROR";
} 



$content = "";
if($status == "OK") {
	$response = wsAddMsg($msg);
	if(empty($response) || $response == null) {
		$form = errorToFormMain($form, "Неизвестная ошибка");	
		$content .= getAddMsgContent($form);
	} else if($response['status'] == STATUS_OK) {
		$content .= getAddMsgSuccessContent();
	} else if($response['status'] == STATUS_FAILED) {
		$form = errorToFormMain($form, $response['descr']);	
		$content .= getAddMsgContent($form);
	} else if($response['status'] == STATUS_NOT_LOGGED_IN) {
		$content .= getLoginContent();
	}
} else {
	$content .= getAddMsgContent($form);
}

print($content);
?>

