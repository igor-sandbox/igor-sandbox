<?php
function navbar($prefix, $menu, $active) {
$items = createItems($prefix, $menu, $active);
$var = <<<HTML
		<div class="navbar navbar-fixed-top">
                        <div class="navbar-inner">
                                <div class="container">
                                        <div class="nav-collapse nav-main">
                                                <a href="/"> <span><i class="nav-home-logged-out"></i></span> </a>
						<ul class="nav pull-right">
							$items
                                                </ul>
                                        </div>
                                </div>
                        </div>
		</div>
HTML;
return $var;
}


function createItems($prefix, $menu, $active) {
	$htmlmenu = "\n";
	foreach ($menu as $key => $value) {
		$htmlmenu.= "<li ";
		$htmlmenu.= $active == $key ? "class=\"active\">\n" : ">\n";
		$htmlmenu.="  <a href=\"$prefix$key\">$value</a>\n</li>\n";
	}
	return $htmlmenu;
}

?>
            
