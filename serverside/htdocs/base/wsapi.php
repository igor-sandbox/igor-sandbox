<?php

define("STATUS_OK", "OK");
define("STATUS_FAILED", "FAILED");
define("STATUS_NOT_LOGGED_IN", "NOT_LOGGED_IN");

define("SNAME", "MIXTURE");

$wsAddr = null;
$wsClient = null;

ini_set("soap.wsdl_cache_enabled", "0");

function wsSetCookie($cookie) {
	setcookie(SNAME, $cookie);
}

function wsGetCookie() {
	if(empty($_COOKIE[SNAME]))
		return;
	return $_COOKIE[SNAME];
}

function wsRemoveCookie() {
	setcookie(SNAME,"",time()-10);
}

function wsInit() {
	global $wsAddr;
	global $wsClient;
	if($wsAddr == null) {
		$wsAddr = "http://localhost:9888/ldvo?wsdl";
	}
        $wsClient = new SoapClient($wsAddr);
        if(!$wsClient) {
                $response['status'] = STATUS_FAILED;
                $response['descr'] = 'Cann\'t create SOAP Client.';
                return $response;
        }
	
        $response['status'] = STATUS_OK;
        $response['descr'] = STATUS_OK;
        return $response;
}

function wsGetClient() {
	global $wsClient;
	if($wsClient == null) {
		wsInit();
	}
	return $wsClient;
}

function wsAddUser($login, $email, $password, $repassword) {
       try {
		$wsargs['arg0'] = $login; 
		$wsargs['arg1'] = $email; 
		$wsargs['arg2'] = $password; 
		$wsargs['arg3'] = $repassword; 

                $client = wsGetClient();
                $wsresponse = $client->addUser($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;
		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}

function wsLoginUser($login, $pass) {
       try {
		$wsargs['arg0'] = $login;
		$wsargs['arg1'] = $pass;

                $client = wsGetClient();
                $wsresponse = $client->loginUser($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;
		if(!empty($wsresponse->return->key)) {
			$response['key'] = $wsresponse->return->key;
			if($response['status'] == STATUS_OK) {
				wsSetCookie($response['key']);
			}
		}

		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}

function wsIsLocalLoggedIn() {
	$cookie = wsGetCookie();
	return !empty($cookie);
}

function wsLogoutUser() {
	try {
		$cookie = wsGetCookie();
		if(empty($cookie)) {
			$response['status'] = STATUS_OK;
			$response['descr'] = "Вы не залогинены";
			return $response;
		}

		$wsargs['arg0'] = $cookie;
                $client = wsGetClient();
                $wsresponse = $client->logoutUser($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;

		if($response['status'] == STATUS_OK) {
			wsRemoveCookie();
		}

		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}

function wsGetMsgs($index, $count) {
	try {
		$cookie = wsGetCookie();
		if(empty($cookie)) {
			$response['status'] = STATUS_NOT_LOGGED_IN;
			$response['descr'] = "Вы не залогинены";
			return $response;
		}

		$wsargs['arg0'] = $cookie;
		$wsargs['arg1'] = $index;
		$wsargs['arg2'] = $count;
                $client = wsGetClient();
                $wsresponse = $client->getMsgs($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;
		if($response['status'] == STATUS_OK) {
			$response['msgs'] = array();
			foreach($wsresponse->return->msgs as $msg) {
				array_push($response['msgs'], $msg);
			}
		}		
		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}

function wsAddMsg($msg) {
	try {
		$cookie = wsGetCookie();
		if(empty($cookie)) {
			$response['status'] = STATUS_NOT_LOGGED_IN;
			$response['descr'] = "Вы не залогинены";
			return $response;
		}

		$wsargs['arg0'] = $cookie;
		$wsargs['arg1'] = $msg;
                $client = wsGetClient();
                $wsresponse = $client->addMsg($wsargs);
		$response['status'] = $wsresponse->return->status;
		$response['descr'] = $wsresponse->return->descr;

		return $response;
        } catch (Exception $ex) {
		$response['status'] = STATUS_FAILED;
		$response['descr'] = "Exception";
		$response['exception'] = $ex;
		return $response;
        }
}

/*$response = wsLoginUser("almer", "carmen1604");
print("Operation finished with results:\n");
print("\tStatus     : ".$response['status']."\n");
print("\tDescription: ".$response['descr']."\n");
var_dump( $_COOKIE);*/


?>
