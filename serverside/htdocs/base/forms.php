<?php

define("FOR_NORMAL", "");
define("FOR_ERROR", "inputError");


function createInputContent($input) {
	$id = $input['id'];
	$help = empty($input['help']) ? " " : $input['help'];
	$name = $input['name'];
	$type = $input['type'];

	$value="";
	if(!empty($input['value'])) {
		$value .= $input['value'];
	}

	$style="";
	if(!empty($input['style'])) {
		$style .= "error";
	}

	$placeholder = empty($input['placeholder']) ? "" : $input['placeholder'];

$var = <<<HTML
<div class="control-group $style">
  <label  class="control-label">$name</label>
    <div class="controls">
      <input id="$id" type="$type" placeholder="$placeholder" value="$value">
      <span class="help-block">$help</span>
    </div>
  </label>
</div>
HTML;

	return $var;
}


function _input($id, $name, $content, $help) {
$var = <<<HTML
<div class="control-group">
  <label  class="control-label">$name</label>
    <div class="controls">
      $content
      <span class="help-block">$help</span>
    </div>
  </label>
</div>
HTML;
return $var;
}


function _inputPass($id, $name, $help, $placeholder) {
$var = <<<HTML
<input id="$id" type="password" placeholder="$placeholder">
HTML;
return _input($id, $name, $var, $help);
}


function _inputText($id, $name, $help, $placeholder) {
$var = <<<HTML
<input id="$id" type="text" placeholder="$placeholder">
HTML;
return _input($id, $name, $var, $help);
}


function _form($action, $title, $submitName, $content) {
$var = <<<HTML
<form id="$action" class="form-horizontal" method="GET" action="$action">
  <fieldset>
    <legend>$title</legend>
    $content
    <div class="control-group">
    	<div class="controls">
   		<button type="submit" class="btn-success">$submitName</button>
    	</div>
    </div>
  </fieldset>
</form>
HTML;
return $var;
}

function createInputsContent($content) {
	$inputs = $content['inputs'];

	$errors = "";
	if(!empty($content['errors'])) {
		$errs = $content['errors'];
		$errors .= "<p class=\"text-error\">\n";
		foreach ($errs as $error) {
			$errors .= "$error<br>\n";
		}
		$errors .= "</p>\n";
	}

	$var = $errors;
	foreach ($inputs as $id => $input) {
		$var.= createInputContent($input);
	}
	return $var;
}

function form($form) {
	$id = $form['id'];
	$action = $form['action'];
	$title = $form['title'];
	$submitName = $form['submitName'];
	$content = createInputsContent($form['content']);

$var = <<<HTML
<form id="$id" class="form-horizontal" method="GET" action="$action">
  <fieldset>
    <legend>$title</legend>
    $content
    <div class="control-group">
    	<div class="controls">
   		<button type="submit" class="btn-success">$submitName</button>
    	</div>
    </div>
  </fieldset>
</form>
HTML;

	return $var;
}



function _registrationForm($opts) {
	$var = "";
	if($opts != null) {
		$var .="<div class=\"text-error\"><span>$opts</span></div>\n";
	}
	$var .= _inputText("login", "Логин", "Логин должен состоять из латинских букв.", "Логин");
	$var .= _inputText("email", "Электронная почта", "Существующий адресс электронной почты.", "");
	$var .= _inputPass("password", "Пароль", "Пароль должен быть не менее 8 символов и быть достаточно надежным.", "");
	$var .= _inputPass("repassword", "Повтор пароля", "Пароли должны совпадать.", "");
	return _form("registration", "Регистрация", "Отправить", $var);
}


function _loginForm() {
	$var .= _inputText("login", "Логин", "", "Логин");
	$var .= _inputPass("password", "Пароль", "", "");
	return _form("login", "Вход", "Войти", $var);
}

function fillInputToForm($form, $id, $text) {
	$form['content']['inputs'][$id]['value'] = $text;
	return $form;
}

function errorToFormMain($form, $error) {
	if(empty($form['content']['errors'])) {
		$form['content']['errors'] = array();
	} 
	
	array_push($form['content']['errors'], $error);	
	return $form;
}

function errorToForm($form, $id, $error) {
	$form['content']['inputs'][$id]['style'] = "error";
	$form['content']['inputs'][$id]['error'] = $error;


	if(empty($form['content']['errors'])) {
		$form['content']['errors'] = array();
	} 
	
	array_push($form['content']['errors'], $error);	
	return $form;
}

function objLoginForm() {
	$inputs = array();

	$inputs["login"] = array(
		"id" => "login",
		"name" => "Логин", 
		"placeholder" => "Логин",
		"type" => "text");

	$inputs["password"] = array(
		"id" => "password",
		"name" => "Пароль", 
		"type" => "password");

	$content = array(
		"inputs" => $inputs,
		"enabled"=> "enabled");

	$form = array(
		"id" => "flogin",
		"action" => "login",
		"title" => "Вход",
		"submitName" => "Войти",
		"content" => $content);
	return $form;
}

function objAddMsgForm() {
	$inputs = array();

	$inputs["msg"] = array(
		"id" => "msg",
		"name" => "Сообщение", 
		"help" => "Введите, пожалуйста, ваше сообщение", 
		"type" => "text");

	$content = array(
		"inputs" => $inputs,
		"enabled"=> "enabled");

	$form = array(
		"id" => "addmsg",
		"action" => "addmsg",
		"title" => "Отправка сообщения",
		"submitName" => "Отправить",
		"content" => $content);
	return $form;
}

function objRegForm() {
	$inputs = array();

	$inputs["login"] = array(
		"id" => "login",
		"name" => "Логин", 
		"help" => "Логин должен состояить из латинских букв", 
		"placeholder" => "Логин",
		"type" => "text");

	$inputs["email"] = array(
		"id" => "email",
		"name" => "Электронная почта", 
		"help" => "Существующий адресс электронной почты", 
		"placeholder" => "Почта",
		"type" => "text");

	$inputs["password"] = array(
		"id" => "password",
		"name" => "Пароль", 
		"help" => "Пароль должен быть достаточно надежным", 
		"type" => "password");

	$inputs["repassword"] = array(
		"id" => "repassword",
		"name" => "Повтор пароля", 
		"help" => "Пароли должны совпадать", 
		"type" => "password");

	$content = array(
		"inputs" => $inputs,
		"enabled"=> "enabled");

	$form = array(
		"id" => "registration",
		"action" => "registration",
		"title" => "Регистрация",
		"submitName" => "Отправить",
		"content" => $content);
	return $form;
}



?>
		
