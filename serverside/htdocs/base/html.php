<?php
function wrapContent($content) {
$var = <<<HTML
<!DOCTYPE html>
<html>
	<head>
	      <title>Metroc</title>
	      <meta name="viewport" content="width=device-width, initial-scale=1.0">        
	      <link rel="stylesheet" media="screen" href="stylesheets/bootstrap.css">
	      <link rel="stylesheet" media="screen" href="stylesheets/main.css">
	      <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	      <!--[if lt IE 9]>
	      <script src="@routes.Assets.at("javascripts/html5.js")" type="text/javascript"></script>
	      <![endif]-->
              <script src="javascripts/jquery-1.8.2.min.js" type="text/javascript"></script>
	      <script src="javascripts/bootstrap.min.js" type="text/javascript"></script>
        </head>
	<body>
	$content
	</body>
</html>    
HTML;
return $var;
}

?>

