<?php

function htmlsc($text)
{
    return htmlspecialchars($text, ENT_QUOTES);
}

function request_var($name, $default=NULL, $row=NULL)
{
	$var = NULL;
		if( isset($_POST[$name]) ) {
	        	$var = $_POST[$name];
	               	if ( get_magic_quotes_gpc() )
	                	$var = stripslashes($var);
	        }
	   	elseif( isset($_GET[$name]) ) {
	          $var = $_GET[$name];
	       if ( get_magic_quotes_gpc() )
	                 $var = stripslashes($var);
	       }
	       elseif( isset($row) ) {
	       if ( is_array($row) && isset($row[$name]) )
	               $var = $row[$name];
	             elseif ( !is_array($row) )
	               $var = $row;
	               else
	       $var = $default;
	} elseif ( isset($default) )
	$var = $default;
	return $var;
}

function short_myself() {
	return $_SERVER['PHP_SELF'];
}

function getservname() {
	return 'http://'.$_SERVER['SERVER_NAME'];
}

function myself() {
	return getservname().short_myself();
}


?>
